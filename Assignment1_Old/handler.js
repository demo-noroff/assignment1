
let bank = new NewBank();
let work = new Work();

const currency = " SEK"

const bankerElement = document.getElementById("bank");
const balanceElement = document.getElementById("balanceID");
const loanElement = document.getElementById("loanButton");
const loanAmountElement = document.getElementById("loanAmount");
const payElement = document.getElementById("payID");
const payButtonElement = document.getElementById("workButton");
const transferButtonElement = document.getElementById("transfer");
const bribeButtonElement = document.getElementById("bribe");
const laptopListElement = document.getElementById("listElement");
const laptopFeaturesElement = document.getElementById("features");
const laptopPriceElement = document.getElementById("price");
const laptopNameElement = document.getElementById("laptopName");
const laptopDescElement = document.getElementById("laptopDesc");
const laptopImageElement = document.getElementById("laptopImage");
const laptopBuyButtonElement = document.getElementById("buy");

let loanHide = loanAmountElement.hidden;
loanHide = true;
loanAmountElement.hidden = true;
bribeButtonElement.hidden = true;


let banker = bank.banker;
let balance = bank.balance;
let loan = bank.currentLoan;
bankerElement.innerText = banker;
InnerText(balanceElement, balance);

// Adding functionality to "Get a loan" Button
loanElement.addEventListener("click", TakeLoan);
function TakeLoan() {
    if (bank.loanGranted === false) { alert("Pay back your outstanding loan!"); return }
    loan = parseInt(prompt("Loan amount: "));
    loanAmountElement.hidden = false;
    bribeButtonElement.hidden = false;
    if(loan > bank.balance*2) { 
        alert("The amount must be less that twice your current balance!");
        loanAmountElement.hidden = true;
        bribeButtonElement.hidden = true;
        return;
    }
    bank.loanGranted = false;
    loanAmountElement.innerText = `Loan ${loan} SEK`;
}

// Add "Work" button functionality
payButtonElement.addEventListener("click", () => {
    work.PayIncrease();
    InnerText(payElement, work.pay);
})

// Add "Bank" button functionality
transferButtonElement.addEventListener("click", () => {
    // Behaviour if the user has a loan
    if(loan>0){
        let temp = work.pay*0.1;
        work.pay-=temp;
        loan = loan - temp;
        InnerText(loanAmountElement, loan);
    }
    bank.balance+=work.pay;
    work.pay = 0;
    InnerText(payElement, work.pay);
    InnerText(balanceElement, bank.balance);
})

bribeButtonElement.addEventListener("click", () => {
    loan-=work.pay;
    work.pay = 0;
    if(loan < 0){
        balanceElement.innerText = bank.balance+=(loan*-1);
        loan = 0;
        InnerText(loanAmountElement, loan);
        bribeButtonElement.hidden = true;
        bank.loanGranted = true;
    }
    else if(loan === 0){
        bribeButtonElement.hidden = true;
        bank.loanGranted = true;
    }
    InnerText(loanAmountElement, loan);
    InnerText(payElement, work.pay);
})


// FETCH FROM API

let laptops = [];
let imageURL = "https://noroff-komputer-store-api.herokuapp.com/"
let testPrice;


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToList(laptops))


// Initial Set-up
laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png";

const addLaptopsToList = (laptops) => {
    laptops.forEach(element => addLaptopToList(element));
}

const addLaptopToList = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopListElement.appendChild(laptopElement);
}

const addFeatures = i =>{
    const selectedLaptop = laptops[i.target.selectedIndex];
    //let tempArray = selectedLaptop.specs;
    /* for(let i=0;i<tempArray.length;i++){
        laptopFeaturesElement.innerText = `Features: ${tempArray[i]}`;
    } */
    laptopFeaturesElement.innerText = `Features: ${selectedLaptop.specs}`;
    laptopNameElement.innerText = selectedLaptop.title;
    laptopDescElement.innerText = `Description: ${selectedLaptop.description}`
    laptopPriceElement.innerText = `Price: ${selectedLaptop.price}`;
    testPrice = selectedLaptop.price;
    laptopImageElement.src = imageURL +selectedLaptop.image;

    
}

// Buy Button functionality
laptopBuyButtonElement.addEventListener("click", () =>{
    let tempPrice = testPrice;
    let tempBalance = parseInt(balanceElement.innerText);
    console.log(tempPrice);
    console.log(tempBalance);
    if(tempBalance >= tempPrice){
        alert(`Thank you for purchasing ${laptopNameElement.innerText}`);
        tempBalance = tempBalance - tempPrice;
        bank.balance = tempBalance;
        InnerText(balanceElement, tempBalance);
    }
    else{
        alert("Insufficient funds!");
    }
})

laptopListElement.addEventListener("change", addFeatures);

function InnerText(element, valueA){
    if(element === loan)
        element.innerText = `Loan ${valueA}${currency}`
    else
        element.innerText = valueA  + currency;
}
