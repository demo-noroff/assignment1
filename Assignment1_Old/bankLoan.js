const Bank = {
    // member variables
    banker: "Joe Banker",
    balance: 200,
    amountOfLoans: 0,
    loan: 0,
    outstandingLoan: struct = {
        visible: false,
        currentLoan: 0
    },

    // member functions
    getAmount: function(){
        return this.balance;
    },
    getLoan: function(){
        return this.outstandingLoan.currentLoan;
    },

    setAmount: function(inputLoanAmount){
        this.balance+=inputLoanAmount;
    },

    // member function constraints
    constraint: function(){
        if(this.outstandingLoan.currentLoan > 0){
            return false;
        }
        else if(this.loan > this.balance*2){
            return false;
        }
        else{
            return true;
        }
    },


}

let test1 = new Bank();

console.log(test1.balance);